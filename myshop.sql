-- 1. Buat database
CREATE DATABASE myshop;
USE myshop;

-- 2. Membuat Table di Dalam Database
-- tabel users
CREATE TABLE users (
  id int(11) PRIMARY KEY AUTO_INCREMENT,
  name varchar(255),
  email varchar(255),
  password varchar(255)
);

-- tabel categories
CREATE TABLE categories (
  id int(11) PRIMARY KEY AUTO_INCREMENT,
  name varchar(255)
);

-- tabel items
CREATE TABLE items (
  id int(11) PRIMARY KEY AUTO_INCREMENT,
  name varchar(255),
  description varchar(255),
  price int(11),
  stock int(11),
  category_id int(11),
  FOREIGN KEY (category_id) REFERENCES categories(id)
);

-- 3. Memasukkan Data pada Table
-- tabel users
INSERT INTO users VALUES (NULL, 'John Doe', 'john@doe.com', 'john123');
INSERT INTO users VALUES (NULL, 'John Doe', 'john@doe.com', 'jenita123');

-- tabel categories
INSERT INTO categories VALUES (NULL, 'gadget');
INSERT INTO categories VALUES (NULL, 'cloth');
INSERT INTO categories VALUES (NULL, 'men');
INSERT INTO categories VALUES (NULL, 'women');
INSERT INTO categories VALUES (NULL, 'branded');

-- tabel items
INSERT INTO items VALUES (NULL, 'Sumsang b50', 'hape keren dari merek sumsang', '4000000', '100', '1');
INSERT INTO items VALUES (NULL, 'Uniklooh', 'baju keren dari brand ternama', '500000', '50', '2');
INSERT INTO items VALUES (NULL, 'IMHO Watch', 'jam tangan anak yang jujur banget', '2000000', '10', '1');

-- 4. Mengambil Data dari Database
-- a. Mengambil data users
SELECT id, name, email FROM users;

-- b. Mengambil data items
SELECT * FROM items WHERE price > 1000000;
SELECT * FROM items WHERE name LIKE '%uniklo%';

-- c. Menampilkan data items join dengan kategori
SELECT items.name, items.description, items.price, items.stock, items.category_id, categories.name as kategori FROM items JOIN categories ON items.category_id = categories.id;

-- 5. Mengubah Data dari Database
UPDATE items SET price = 2500000 WHERE name = 'sumsang b50';